Feature: The internet website

    Scenario: As a user, I can log into seceure area

    Given I am on the "login" page
    When I login with "<email>" and "<password>"
    Then I should see a message saying  "<message>"

    Examples:
        | email                   | password  | message                                   |
        | tikotadeo@gmail.com     | patas500  | Welcome to your Jetstream application!    |
        | user@bat.com            | 123456    | Whoops! Something went wrong.             |
